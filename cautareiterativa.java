package cautari;

public class CautareBinaraIterativ {

	public static void main(String[] args) {
		int[] vector= {-1,0,8,9,10};
		System.out.println(cautareBinaraIterativ(vector,8));

	}
	
	public static int cautareBinaraIterativ(int[] v,int e)
	{
		int left=0;
		int right=v.length-1;
		while(left<=right)
		{
			int mid=(left+right)/2;
			if(v[mid]==e)
				return mid;
			if(v[mid]<e)
				left=mid+1;
			else right=mid-1;
		}
		return -1;			
			
	}

}
