package sortari;

import java.util.Arrays;

public class QuickS {

	public static void main(String[] args) {
		int[] v= {89,78,-66,55,444,33,12};
		quicks(v,0,v.length-1);
		System.out.println(Arrays.toString(v));

	}

	public static int partition(int[] v, int stg, int dr)
	{
		int pivot=v[dr];
		int i=stg-1;
		for(int j=stg;j<dr;j++)
			if(v[j]<pivot)
			{
				i++;
				int temp=v[i];
				v[i]=v[j];
				v[j]=temp;
			}
		int temp=v[i+1];
		v[i+1]=v[dr];
		v[dr]=temp;
		return i+1;
	}

	public static void quicks(int[] v, int stg, int dr) {
		if(stg<dr)
		{
			int pivotIndex=partition(v,stg,dr);
			quicks(v,stg,pivotIndex-1);
			quicks(v,pivotIndex+1,dr);
		}
	}

}
