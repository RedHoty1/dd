package sortari;

import java.util.Arrays;

public class Selection {

	public static void main(String[] args) {
		int[] v= {170,35,90,2,45};
		selection(v);
		System.out.println(Arrays.toString(v));

	}

	static void selection(int[] v) {

		for(int i=0;i<v.length-1;i++) {
			int pozMin=i;
			for(int j=i+1;j<v.length;j++) {
				if(v[j]<v[pozMin])
				{
					pozMin=j;
				}}
			int temp=v[pozMin];
			v[pozMin]=v[i];
			v[i]=temp;
		}
	}

}
