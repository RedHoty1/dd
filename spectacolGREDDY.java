package greedy;

import java.util.ArrayList;

public class ProgramSpectacole {

	public static void main(String[] args) {
		Spectacol s1=new Spectacol(12,12.30);
		Spectacol s2=new Spectacol(15,18.15);
		Spectacol s3=new Spectacol(15.16,16);
		Spectacol s4=new Spectacol(18,20);
		Spectacol[] s= {s1,s2,s3,s4};
		ArrayList<Spectacol> rezultat=getSpectacole(s);
		for(int i=0;i<rezultat.size();i++)
			System.out.println(rezultat.get(i).start+
					"...."+rezultat.get(i).finish
					);
	}
	
	static ArrayList<Spectacol> getSpectacole(Spectacol[] specs){
		sort(specs);
		ArrayList<Spectacol> deR=new ArrayList<Spectacol>();
		deR.add(specs[0]);
		for(int i=1;i<specs.length;i++)
		{
			if(specs[i].start>=deR.get(deR.size()-1).finish)
				deR.add(specs[i]);
		}
		return deR;
	}
	
	static void sort(Spectacol[] spects) {
		boolean swap;
		for(int i=0;i<spects.length;i++)
		{
			swap=false;
			for(int j=0;j<spects.length-i-1;j++)
			{
				if(spects[j+1].finish<spects[j].finish)
				{
					Spectacol temp=spects[j+1];
					spects[j+1]=spects[j];
					spects[j]=temp;
					swap=true;
				}
			}
			if(swap==false)
				break;
		}
	}
	
	static class Spectacol{
		double start;
		double finish;
		public Spectacol(double start, double finish) {
			super();
			this.start = start;
			this.finish = finish;
		}
		
	}

}

