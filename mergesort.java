package sortari;

import java.util.Arrays;

public class Merge {

	public static void main(String[] args) {
		int[] e= {8,-2,70,6,0,-4,9};
		sortM(e,0,e.length-1);
		System.out.println(Arrays.toString(e));

	}
	
	public static void sortM(int[] v, int l, int r)
	{
		if(l<r)
		{
			int m=l+(r-l)/2;
			sortM(v,l,m);
			sortM(v,m+1,r);
			merge(v,l,m,r);
		}
	}
	
	public static void merge(int[] v,int l, int m, int r)
	{
		int n1=m-l+1;
		int n2=r-m;
		int[] L=new int[n1];
		int[] R=new int[n2];
		for(int i=0;i<n1;i++)
			L[i]=v[l+i];
		for(int j=0;j<n2;j++)
			R[j]=v[m+1+j];
		int i=0,j=0;
		int k=l;
		while(i<n1&&j<n2)
		{
			if(L[i]<R[j])
			{
				v[k]=L[i];
				i++;
			}
			else {
				v[k]=R[j];
				j++;
			}
			k++;
		}
		while(i<n1)
		{
			v[k]=L[i];
			i++;
			k++;
		}
		while(j<n2)
		{v[k]=R[j];
		j++;
		k++;
		}
	}

}
