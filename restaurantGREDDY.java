package greedy;

public class Restaurant {

	public static void main(String[] args) {
		int[] timpi= {10,20,10,20};
		int[] rezultat=getMin(timpi,20);
		for(int i=0;i<rezultat.length;i++)
			System.out.println(rezultat[i]);

	}
	
	static int[] getMin(int[] timpi, int timpPersoana) {
		int[] deR=new int[timpi.length];
	//	Specialitate[] specs=new Specialitate[timpi.length];
		
		sort(timpi);
		for(int i=0;i<timpi.length;i++)
		{
			if(timpPersoana>=timpi[i]) {
				timpPersoana-=timpi[i];
				deR[i]=timpi[i];
			}
		}
		return deR;
			
	}
	
	static void sort(int[] elemente) {
		boolean swap;
		for(int i=0;i<elemente.length;i++)
		{
			swap=false;
			for(int j=0;j<elemente.length-i-1;j++) {
			if(elemente[j+1]<elemente[j])
			{
				int temp=elemente[j];
				elemente[j]=elemente[j+1];
				elemente[j+1]=temp;
				swap=true;
			}
			}
			if(swap==false)
				break;
		}
	}
	/*
	static class Specialitate{
		int timp;

		public Specialitate(int timp) {
			super();
			this.timp = timp;
		}
		
	}*/

}
