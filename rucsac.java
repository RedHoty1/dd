package greedy;


public class Rucsac {

	public static void main(String[] args) {
		int[] greutati= {12,1,2,4,1};
		int[] valori= {4,2,2,10,1};
		Lingou[] rez=getLingouriInRucsac(greutati,valori, 15);
		for(int i=0;i<rez.length;i++)
			System.out.println(rez[i].greutate+"-----"+rez[i].valoare);
	}
	
	static void sort(Lingou[] lingouri) {
		for(int i=0;i<lingouri.length-1;i++)
			for(int j=0;j<lingouri.length-i-1;j++)
				if(lingouri[j].castig<lingouri[j+1].castig)
				{
					Lingou temp=lingouri[j];
					lingouri[j]=lingouri[j+1];
					lingouri[j+1]=temp;
				}

	}
	static Lingou[] getLingouriInRucsac(int[] greutati, int[] valori, double capacitateRucsac) {
		Lingou[] deLucru=new Lingou[greutati.length];
		Lingou[] deR=new Lingou[greutati.length];
		for(int i=0;i<greutati.length;i++)
			deLucru[i]=new Lingou(greutati[i],valori[i]);
		sort(deLucru);
		for(int i=0;i<deLucru.length;i++) {			
			double greutateLingouCurent=deLucru[i].greutate;
			double valoareLingouCurent=deLucru[i].valoare;
			if(capacitateRucsac>=greutateLingouCurent)
			{
				deR[i]=deLucru[i];
				capacitateRucsac-=greutateLingouCurent;
			}
			else
			{
				double fractie=valoareLingouCurent/greutateLingouCurent;
				Lingou l=new Lingou(capacitateRucsac,capacitateRucsac*fractie);
				deR[i]=l;
				capacitateRucsac-=l.greutate;
			
			}
		}
		return deR;
	}
	
	static class Lingou{
		double greutate;
		double valoare;
		double castig;
		public Lingou(double greutate, double valoare) {
			super();
			this.greutate = greutate;
			this.valoare = valoare;
			this.castig=this.valoare/this.greutate;
		}
		
	}

}
