public class CautariBinare {
public static void main(String[] args) {
BTNode nod1, nod2, nod3, nod4, nod5, nod6, nod7, nod8;
nod8 = new BTNode(20, null, null);
nod7 = new BTNode(17, null, nod8);
nod6 = new BTNode(3, null, null);
nod5 = new BTNode(9, nod6,nod7);
nod4 = new BTNode(53, null, null);
nod3 = new BTNode(59, null, null);
nod2 = new BTNode(55, nod4, nod3);
nod1 = new BTNode(45,nod5, nod2);
BTNode nodCautat = search(nod1, 9);
if(nodCautat==null)
System.out.println("Valoarea cautata nu este in arborele binar de cautare");
else
System.out.println("S-a gasit valoarea cautata: " + nodCautat.getData());
}
public static BTNode search(BTNode node, int k)
{
if(null==node || (int)node.data==k)
return node;
if(k<(int)node.data)
return search(node.getLeft(), k);
else
return search(node.getRight(), k);
}
}
